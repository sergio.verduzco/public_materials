This folder contains source code that reproduces the figures for the paper:
"Self-configuring feedback loops for sensorimotor control" by
Sergio Verduzco-Flores and Erik De Schutter.

Four Jupyter notebook files and one Python file are included. 

The prerequistes for running this code are:
* Python 3.5 or above (https://www.python.org)
* Jupyter (https://jupyter.org)
* Draculab (https://gitlab.com/sergio.verduzco/draculab)

The included files are:

## source_code_1.ipynb  
Executing all the cells in this notebook can produce figures 2-8 of the paper.
In addition, configurations of the network not shown in the main text of the paper can also be visualized.

## source_code_2.ipynb  
Executing this notebook will produce various versions of figure 9, and panel B of figure 5.

## source_code_2B.ipynb  
Executes the same simulations as in source_code_2, but with the synergistic
configuration of the paper.

## source_code_3.ipynb
A parallel version of the simulation in source_code1.ipynb.

## net_builders.py
This auxiliary Python script produces Draculab networks for the simulation in `source_code_1.ipynb`. The extra Python dictionaries containing parameter values can be consulted here. The `net_from_cfg` method creates the network with the standard spinal cord configuration, whereas the `syne_net` method creates it for the "synergistic" configuration.

---
The mp4 files contain the supplementary videos for the paper.


