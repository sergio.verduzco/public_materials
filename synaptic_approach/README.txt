This folder contains the source code for the paper:
"A differential Hebbian framework for biologically-plausible motor control" by
Sergio Verduzco, William Dorrell, and Erik De Schutter.

The source code consists of 7 Jupyter notebooks, and one Python file.

The prerequistes for running this code are:
* Python 3.5 or above (https://www.python.org)
* Jupyter (https://jupyter.org)
* Draculab (https://gitlab.com/sergio.verduzco/draculab)

The included files are:

nn_linearA.ipynb
This notebook runs parallel simulations of the model in figure 2 of the paper,
in order to produce panel A.

nn_linearA2.ipynb
Runs parallel simulations of the model in figure 2, but only for the synaptic
plasticity rule in equation B.2 . This is used to produce the data in the
corresponding plot of figure B.10 .

nn_linearBC.ipynb
Runs the simualtion to produce panels B and C in figure 2.

nn_pend_1.ipynb
Runs the simulation that produces figures 4 and D.11 .

nn_pend2_par.ipynb
Runs parallel simuations of the model in figure 7, in order to produce panels A,
B, and C.

nn_pend2.ipynb
Runs simualations to produce panels D-G in figure 7.

rl5E_lite_from_cfg.py
This is an auxiliary Python script for nn_pend2.ipynb and nn_pend2_par.ipynb .
The script receives a parameter dictionary, and builds a Draculab network using
those parameters.

nn_pend3.ipynb
Produces panels B, C, and D of figure 8.

nn_pend4.ipynb
Hyperparameter search for the models in nn_pend2, nn_pend2_par using a genetic algorithm.
