The `synaptic_approach` folder contains source code for:
Verduzco-Flores et al. 2022, A differential Hebbian framework for biologically-plausible motor control.

The `adaptive_plasticity` folder contains source code for:
Verduzco-Flores and De Schutter 2022, Self-configuring feedback loops for sensorimotor control.
