#! /usr/bin/env python3
# coding: utf-8

# # t2p3_runner.py
# 
# A notebook to sequentially run different configurations of tests 
# for the rga, rga_normal, and rga_21 rules.
# 
# Usage:
# $t2p3_runner.py seed
# where 'seed' is an integer used to seed numpy's random generator

if __name__=='__main__':

    from draculab import *
    import numpy as np
    
    import matplotlib.pyplot as plt
    import time
    import sys
    
    seed = 3
    argv = sys.argv
    if len(argv) != 2:
        raise ValueError('t2p3_runner receives a single integer argument')
    try:
        seed = int(argv[1])
    except ValueError:
        print('Incorrect argument. Using default seed.')
    
    np.random.seed(seed)
    
    # A function to create Haar matrices, used for connections from controllers to the plant
    def haar_matrix(dim):
        """ Returns an array with the normalized (2**dim x 2**dim) Haar matrix.
        
            Args:
                dim: an integer.
            Returns:
                A 2D numpy array of shape (2**dim, 2**dim), where each row is a
                Haar vector of norm 1.
        """
        # Obtain the matrix using the iterative procedure with Kronecker products
        h0 = np.array([1., 1.])
        h1 = np.array([1., -1.])
        haar = np.concatenate(([h0], [h1]))
        for d in range(1,dim):
            haar = np.concatenate((np.kron(haar, h0), 
                                np.kron(np.identity(2**d), h1)), axis=0)
        # Normalize the rows of the Haar matrix
        for idx, row in enumerate(haar):
            haar[idx, :] = row / np.linalg.norm(row)
        return haar
    
    
    def create_t2p3_net(params):
        """ Returns a network with the given configuration.
        
            Args:
                params: a parameter dictionary with these entries:
                N = size of each population (integer)
                t_pres = duration of each set of target values (float)
                M__P_type = type of the M to P connections. A string, either
                        'identity', 'haar', 'overcomplte', or 'overcomplete2'
                M_type = type of the units in the M population. A string, either
                    'am', am2D', 'am_pm', or 'am_pulse'.
                SPF__M_syn_type = type of the synapse from SPF to M (synapse)
                om_var = omega frequency heterogeneity (float)
                track_SPF__M_w = Boolean. Whether to track SPF__M weight for 1 M(E|I) unit
            Returns:
                net: a draculab network with the given configuration
                pops: a dictionary. Each entry has the name of a population, and
                    a list with the identifiers of units in that population.
                """
        N = params['N']
        t_pres = params['t_pres']
        M__P_type = params['M__P_type']
        M_type = params['M_type']
        SPF__M_syn_type = params['SPF__M_syn_type']
        om_var = params['om_var']
        track_SPF__M_w = params['track_SPF__M_w']
        #--------------------------------------------------------------
        M__P_mats = {'identity' : 1.*np.eye(N),
                    'haar' : haar_matrix(int(np.round(np.log(N)/np.log(2.)))),
                    'overcomplete2' : np.random.random((N, 3*N))}
        if N > 1:
            M__P_mats['overcomplete'] = np.concatenate((M__P_mats['identity'], M__P_mats['haar']), axis=1)
        norms = np.linalg.norm(M__P_mats['overcomplete2'], axis=0)
        M__P_mats['overcomplete2'] = M__P_mats['overcomplete2']/norms
        W = M__P_mats[M__P_type]
        M_N = W.shape[1]
    
    
        des_pat = np.zeros(N) # desired pattern in SP
        des_pats = np.random.random((2000, N)) # many desired patterns for SP
        des_pat[np.arange(0,N,2)] = .9 # a 'one' every other entry
        par_heter = 0.1 # range of heterogeneity as a fraction of the original value
        randz = lambda : (1. + par_heter*(np.random.rand(N)-0.5))
    
        net_params = {'min_delay' : 0.002,
                    'min_buff_size' : 10 }
        SF_params = {'type' : unit_types.sigmoidal,
                    'thresh' : 0. * randz(),
                    'slope' : 1. * randz(),
                    'init_val' : 0.2 * randz(),
                    'tau' : 0.05 }#* randz() }
        SP_params = {'type' : unit_types.source,
                    'init_val' : 0.5,
                    'function' : lambda x: None }
        SPF1_params = {'type' : unit_types.out_norm_sig,
                    'thresh' : 0.4 * randz(), # 0.5 originally
                    'slope' : 4. * randz(),  # 4. originally
                    'delay' : 0.3,
                    'init_val' : 0.3 * randz(),
                    'tau_fast': 0.01, # 0.005
                    'tau_mid': 0.2,  # 0.05
                    'tau_slow' : 10., # 5
                    'tau' : 0.05, #* randz(),
                    'des_out_w_abs_sum' : 1. }
        SPF2_params = SPF1_params
        P_params = {'type' : unit_types.linear,
                    'init_val' : 0.,
                    'tau' : 0.05 }#* randz() }
    
        if M_type == "am_pm":
            init_base = np.array([0.5, 0.5, 2.*np.pi, 0.5])
            M_syn_type = unit_types.am_pm_oscillator
        elif M_type == "am2D":
            init_base = np.array([0.5, 0.5])
            M_syn_type = unit_types.am_oscillator2D 
        elif M_type == "am":
            init_base = np.array([0.5, 0.5, 0.5])
            M_syn_type = unit_types.am_oscillator 
        M_params = {'type' : M_syn_type,
                    'integ_meth' : 'odeint',
                    'tau_u' : 0.1,
                    'tau_c' : .1, # originally 0.2
                    'init_val' : [(r+1.)*init_base for r in 0.2*(np.random.random(M_N)-0.5)],
                    'multidim' : True,
                    'n_ports' : 3,
                    'omega' : 2.*np.pi,
                    'custom_inp_del' : 200,
                    'tau_fast': 0.005,
                    'tau_mid' : 0.05,
                    'tau_slow' : 1.,
                    'delay' : 0.24,
                    'A' : 1.,
                    'mu' : 0.,
                    'sigma' : 0.0 }
    
        def create_freqs_steps(n, w, r):
            """ Returns a 2-tuple with the lists required for heterogeneous frequencies.
    
                Args:
                    n : number of units
                    w : base angular (rad/s)
                    r : amplitude of noise
                Returns
                    2-tuple : (freqs, steps)
                    freqs : a list with n angular frequencies.
                    steps : a list with the corresponding delays. 
            """
            ws = w + r*(np.random.random(n) - 0.5)
            #ws = ws / M_params['tau_t'] # angular frequencies
            mp_del = np.arctan(P_params['tau']*ws)/ws
            psf_del = np.arctan(SF_params['tau']*ws)/ws
            sfspf_del = np.arctan(SPF1_params['tau']*ws)/ws
            spfm_del1 = np.arctan(M_params['tau_c']*ws)/ws
            spfm_del2 = np.arctan(M_params['tau_u']*ws)/ws
            d1 = mp_del + psf_del + sfspf_del + 4.*0.02
            del_steps = [int(d) for d in np.ceil(d1/net_params['min_delay'])]
            return (list(ws), del_steps)
    
        om_var = 2. # increases omega frequency heterogeneity
    
        # creating units
        net = network(net_params)
        SF = net.create(N, SF_params)
        SP = net.create(N, SP_params)
        SPF1 = net.create(N, SPF1_params)
        SPF2 = net.create(N, SPF2_params)
        P = net.create(N, P_params)
    
        omegasE, del_stepsE = create_freqs_steps(M_N, M_params['omega'], om_var)
        M_params['omega'] = omegasE
        M_params['custom_inp_del'] = del_stepsE
        ME = net.create(M_N, M_params)
    
        omegasI, del_stepsI = create_freqs_steps(M_N, M_params['omega'], om_var)
        M_params['omega'] = omegasI
        M_params['custom_inp_del'] = del_stepsI
        MI = net.create(M_N, M_params)
        
        pops = {'SF':SF, 'SP':SP, 'SPF1':SPF1, 'SPF2':SPF2, 'P':P, 'ME':ME, 'MI':MI}
    
        # set the pattern in SP
        def make_fun(idx):
            #return lambda t: des_pat[idx]
            return lambda t: des_pats[int(round(t/t_pres)),idx]
        for idx, u in enumerate(SP):
            net.units[u].set_function(make_fun(idx))
    
        # Create the connections. See note on "detecting layer distances" in cortex wiki
        SF__SPF1_conn = {'rule' : "one_to_one",
                        'delay' : 0.02 }
        SF__SPF2_conn = SF__SPF1_conn
        SF__SPF1_syn = {'type' : synapse_types.static,
                    'init_w' : -1. }
        SF__SPF2_syn = {'type' : synapse_types.static,
                    'init_w' : 1. }
        SP__SPF1_conn = {'rule' : "one_to_one",
                        'delay' : 0.02 }
        SP__SPF2_conn = SP__SPF1_conn
        SP__SPF1_syn = {'type' : synapse_types.static,
                    'init_w' : 1. }
        SP__SPF2_syn = {'type' : synapse_types.static,
                    'init_w' : -1. }
        SPF1__ME_conn = {'rule': 'all_to_all',
                        'delay': 0.02 }
        SPF2__ME_conn = SPF1__ME_conn
        SPF1__MI_conn = SPF1__ME_conn
        SPF2__MI_conn = SPF1__ME_conn
        SPF1__ME_syn = {'type' : SPF__M_syn_type,
                        'lrate': 50.,
                        #'post_delay': del_steps,
                        'inp_ports': 0,
                        'max_w' : 2.,
                        'min_w' : 0.,
                        'sig1' : .2,
                        'sig2' : .2,
                        'init_w' : {'distribution':'uniform', 'low':0.05, 'high':.1} }
        SPF2__ME_syn = SPF1__ME_syn
        SPF1__MI_syn = SPF1__ME_syn
        SPF2__MI_syn = SPF1__ME_syn
    
        # connect ME/MI to P
        ME__P_conn = {'rule': 'all_to_all',
                    'delay': 0.02 }
        ME__P_syn = {'type': synapse_types.static_l0_normal,
                    'w_sum' : 2.,
                    'tau_norml' : 5.,
                    'init_w' : W.flatten() }
        MI__P_conn = ME__P_conn
        MI__P_syn = {'type': synapse_types.static_l0_normal,
                    'w_sum' : 2.,
                    'tau_norml' : 5.,
                    'init_w' : -W.flatten() }
        # From P to SF
        P__SF_conn = {'rule' : 'one_to_one',
                    'delay' : 0.02 }
        P__SF_syn = {'type' : synapse_types.static,
                    'init_w' : 3. }
        # lateral connections in M
        ME__ME_conn = {'rule': 'all_to_all',
                    'allow_autapses' : False,
                    'delay' : 0.02 }
        ME__ME_syn = {'type' : synapse_types.static,
                    'lrate' : 0.1,
                    'inp_ports': 1,
                    'init_w' : -0.02/N }
        ME__MI_conn = {'rule': 'one_to_one',
                    'delay' : 0.02 }
        ME__MI_syn = {'type' : synapse_types.static,
                    'inp_ports': 1,
                    'init_w' : -0.5 } # -0.5 }
        MI__ME_conn = ME__MI_conn
        MI__ME_syn = ME__MI_syn
        MI__MI_conn = ME__ME_conn
        MI__MI_syn = ME__ME_syn
    
        net.connect(SF, SPF1, SF__SPF1_conn, SF__SPF1_syn)
        net.connect(SF, SPF2, SF__SPF2_conn, SF__SPF2_syn)
        net.connect(SP, SPF1, SP__SPF1_conn, SP__SPF1_syn)
        net.connect(SP, SPF2, SP__SPF2_conn, SP__SPF2_syn)
        net.connect(SPF1, ME, SPF1__ME_conn, SPF1__ME_syn)
        net.connect(SPF1, MI, SPF1__MI_conn, SPF1__MI_syn)
        net.connect(SPF2, ME, SPF2__ME_conn, SPF2__ME_syn)
        net.connect(SPF2, MI, SPF2__MI_conn, SPF2__MI_syn)
        net.connect(ME, P, ME__P_conn, ME__P_syn)
        net.connect(MI, P, MI__P_conn, MI__P_syn)
        net.connect(P, SF, P__SF_conn, P__SF_syn)
        net.connect(ME, ME, ME__ME_conn, ME__ME_syn)
        net.connect(MI, MI, MI__MI_conn, MI__MI_syn)
        net.connect(ME, MI, ME__MI_conn, ME__MI_syn)
        net.connect(MI, ME, MI__ME_conn, MI__ME_syn)
    
        # tracking state variables of the am_pm_oscillator
        track_params = {'type' : unit_types.source,
                        'init_val' : 0.02,
                        'function' : lambda t: None }
        def create_state_track(uid, var_id):
            return lambda t: net.units[uid].buffer[var_id,-1]
        if M_type == "am":
            n_track = 3
        elif M_type in ["am2D", "am_pulse"]:
            n_track = 2
        elif M_type == "am_pm":
            n_track = 4
        M_track = net.create(n_track, track_params)
        for var in range(n_track):
            net.units[M_track[var]].set_function(create_state_track(ME[0],var))
        pops['M_track'] = M_track
        
        if track_SPF__M_w:
            # tracking SPF__ME weights
            SPF__M_track = net.create(2*N, track_params)
            SPF__M_syns = [syn for syn in net.syns[ME[0]] if syn.port == SPF1__ME_syn['inp_ports']]
            def SPF__M_fun(idx):
                """ Creates a function to track a weight from SPF to ME. """
                return lambda t: SPF__M_syns[idx].w
            for idx in range(len(SPF1+SPF2)):
                net.units[SPF__M_track[idx]].set_function(SPF__M_fun(idx))
            pops['SPF__M_track'] = SPF__M_track
            
        return net, pops
    
    
    ############################
    #  Run all configurations  #
    ############################
    sim_time = 300 
    N_l = [1, 2, 4, 8]
    M__P_type_l = ['identity', 'haar', 'overcomplete', 'overcomplete2']
    SPF__M_syn_type_l = [synapse_types.rga, synapse_types.rga_21] # rga, normal_rga, rga_21
    
    M_type = "am2D"
    t_pres = 50.
    om_var = 1.
    track_SPF__M_w = False # whether to track SPF__M weight for 1 M(E|I) unit
    
    results = []
    
    for N in N_l:
        for SPF__M_syn_type in SPF__M_syn_type_l:
            for M__P_type in M__P_type_l:
                if N==1 and (M__P_type == 'haar' or M__P_type == 'overcomplete'):
                    break
                params = {'N' : N,
                        't_pres' : t_pres,
                        'om_var' : om_var,
                        'track_SPF__M_w' : track_SPF__M_w,
                        'M__P_type' : M__P_type,
                        'SPF__M_syn_type' : SPF__M_syn_type,
                        'M_type' : M_type }
                net, pops = create_t2p3_net(params)
                start_time = time.time()
                times, data, _  = net.flat_run(sim_time)
                print(params)
                print('Execution time is %s seconds' % (time.time() - start_time))
                data = np.array(data)
                results.append((params, pops, times, data))
    
    # save the results
    """
    import pickle
    fname = time.strftime('t2p3_results_%Y-%m-%d_%H:%M', time.localtime())
    with open(fname, 'wb') as f:
        pickle.dump(results, f)
        f.close()
    """
    
    # Create a matrix of results for each of the learning rules
    # The measure we will use will be the mean value of the norm of SP-SF 
    # (both vectors normalized) for the last half of the simulation
    
    # rows will be number of units, columns the M__P connection type
    rga_res = np.zeros((len(N_l), len(M__P_type_l)))
    rga_21_res = np.zeros((len(N_l), len(M__P_type_l)))
    rga_ge_res = np.zeros((len(N_l), len(M__P_type_l)))
    static_res = np.zeros((len(N_l), len(M__P_type_l)))
    
    ct2idx = {'identity' : 0,
            'haar': 1,
            'overcomplete' : 2,
            'overcomplete2': 3 }
    N2idx = [None, 0, 1, None, 2, None, None, None, 3]
    
    for conf, pops, times, data in results:
        init_idx = int(np.round(len(times)/2.))
        SP_data = np.array(data[pops['SP']])
        SF_data = np.array(data[pops['SF']])
        if conf['N'] > 1:
            SP_data_unit = (SP_data/np.linalg.norm(SP_data, axis=0))
            SF_data_unit = (SF_data/np.linalg.norm(SF_data, axis=0))
            SP_SF = SP_data_unit - SF_data_unit
        else:
            SP_SF = SP_data - SF_data
        SP_SF_norm = np.linalg.norm(SP_SF, axis=0)
        mean_error = np.mean(SP_SF_norm[init_idx:])
        if conf['SPF__M_syn_type'] == synapse_types.rga:
            rga_res[N2idx[conf['N']]][ct2idx[conf['M__P_type']]] = mean_error
        elif conf['SPF__M_syn_type'] == synapse_types.rga_21:
            rga_21_res[N2idx[conf['N']]][ct2idx[conf['M__P_type']]] = mean_error
        elif conf['SPF__M_syn_type'] == synapse_types.rga_ge:
            rga_ge_res[N2idx[conf['N']]][ct2idx[conf['M__P_type']]] = mean_error
        elif conf['SPF__M_syn_type'] == synapse_types.static:
            static_res[N2idx[conf['N']]][ct2idx[conf['M__P_type']]] = mean_error
            
    # save the error matrices
    import pickle
    mat_dict = {'rga' : rga_res,
                'rga_21' : rga_21_res,
                'rga_ge' : rga_ge_res,
                'static' : static_res }
    #fname = time.strftime('t2p3_mats' + str(seed) + '_%Y-%m-%d_%H:%M', time.localtime())
    fname = 't2p3_mats' + str(seed) 

    with open(fname, 'wb') as f:
        pickle.dump(mat_dict, f)
        f.close()
    
    print("Successfully finished run %d of t2p3_runner" % (seed))
