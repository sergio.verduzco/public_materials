%% FixedPointController.m
%
% Here we implement a neurally plausible RL agent that learns to control a
% flat pendulum with friction.
%
% It does this using an actor-critic architecture. It learns both a value
% neuron and a policy from one state representation. The value represents
% the standard discounted future reward and is learnt through standard
% dopamine type setup. 
%
% The policy is implemented by a collection of neurons that code for a
% goal angle. A feedback controller then takes the goal angle and the
% current angle and applies proportional control to track towards the goal
% angle. As the angle variable is periodic it needs to apply torques that 
% are not just proportional to theta_goal - theta, but the actual distance
% between theta_goal and theta on the circle. This is difficult to
% implement neurally. We did it here by learning an additional variable,
% the control variable phi. You'll notice that theta_goal - theta is equal
% to the distance along the circle between theta_goal and theta for parts 
% of the circle far from the discontinuity (which is around 360 & 0 
% degrees). We therefore use phi as a code for two different circular 
% co-ordinate systems. Our agent then learns which co-ord system to use at 
% for each point in the input space, i.e. it learns to use the controller
% furthest from its discontinuity. The discontinuities for the two circles
% are placed 180 apart.
%
% The weights are learnt through a TD error modulated hebbian rule
%
% Delta_Weight = Step*Presynaptic_Activity*Postsynaptic_Activity*TD_Error
%
% During first half of training random goal angles are chosen and the
% controller & value weights are learnt. Then in the second half the
% controller choice is fixed and the goal angle & value weights are learnt.
%
% Part of Veduzco-Flores et al. 2020, Neurips submission, An approach to 
% synaptic learning for autonomous feedback control

% I run multiple times for each parameter setup, this controls how many
Runs = 3;

% There are two different setups, a training regime (Train = 1) and a
% testing. During training there is a lot of noise, that is then removed
% for testing (Train = 0). Choose first train = 1, then train = 0.
Train = 1;
if Train == 1
    Reintialise = 1; % Setup new weights
    Learn = 1; % Synaptic learning on
    N = 500001; % Number of steps to run
    sigma_scaler_Z = 30; % Noise level for angle policy
    sigma_scaler_P = 0.05; % Noise level for controller policy
else
    Reintialise = 0; % Keep same weights
    Learn = 0; % No more learning, testing now
    N = 50001; % Number of steps to run
    sigma_scaler_Z = 0;  % Turn off noise
    sigma_scaler_P = 0;
end

% Setup Parameters
Alpha_W0 = 0.005; % Learning Rate for value function
Alpha_P0 = 0.0005; % Learning rate for control variable
Alpha_Z0 = 0.005; % Learning Rate for goal angle
Gamma = 0.99; % Reward Discount
Th_Num = 10; % Number of state neurons per angle
Max_Torque = 1; % Maximum torque, 1 means speed can change by 1 deg/timestep^2 at most
eta = 0.1; % Viscous drag
S_Theta = 60; % Width of tuning curved for input layer
Theta_Thresh = 5; % Threshold for reaching goal angle and choosing another
T_Thresh = 30; % Threshold # timesteps for 'you took too long, choose another angle'
Step_Size = 2; % Target angle's random walk step size

% If you are reinitialising create a new storage container for weights 
if Train
    Weight_Store = cell(Runs, 3);
    Weight_Store_Init = cell(Runs, 3);
end

% Choose representation characteristics
SI_Theta = 1/S_Theta^2;
Theta_Cent = -180:360/(Th_Num):180;
Theta_Cent = Theta_Cent(1:Th_Num);
Rewards_End = zeros([Runs,1]);

% Set up some plotting things
Th_Eval_Num = 30;
Th_Grid = -180:360/(Th_Eval_Num-1):180;
Th_Grid = [Th_Grid; mod(Th_Grid + 360, 360) - 180];
Num_Ticks = 6;
Theta_Ticks = 0:Th_Eval_Num/(Num_Ticks-1):Th_Eval_Num;
Theta_Tick_Labels = Theta_Ticks*360/Th_Eval_Num - 180;
Rows = Runs;
Columns = 7;
figure
% I bin the reward with these
Binning = 100;
Bins = 1:N/Binning:(N+1);
Bins = round(Bins);
Bin_Centres = Bins(1:Binning) + N/(2*Binning);

for run = 1:Runs
    Alpha_W = Alpha_W0;
    Alpha_P = Alpha_P0;
    Alpha_Z = 0; % Don't learn these weights in first half
    
    % During intial training we stop and choose a new random angle when we
    % receive reward, this stops happening in the second half or when
    % testing.
    if Train == 0
        limit = 2;
    else
        limit = 1;
    end
    
    % Intialise variables
    Theta = zeros([N+1, 2]);
    Theta_D = zeros([N+1, 1]);
    Theta_G = zeros([N+1, 1]);
    Omega = zeros([N+1, 1]);
    Torque = zeros([N+1, 1]);
    Delta_W = zeros([N+1, 1]);
    Delta_Z = zeros([N+1, 1]);
    Delta_P = zeros([N+1, 1]);
    Reward = zeros([N+1, 1]);
    Input_Old = zeros([Th_Num^2, 1]);
    Input_New = zeros([Th_Num^2, 1]);
    Input_NW = zeros([Th_Num^2, 1]);
    Out_New_Z = zeros([1, Th_Num]);
    Out_Old_Z = zeros([1, Th_Num]);
    Outputs = zeros([N+1, 1]);
    th = zeros([N+1,1]);
    V_Old = 0;
    Phi = zeros([N+1, 1]); % Control variable

    % Get weights either intiaised or loaded from storage
    if Reintialise
        Z = randn([Th_Num, Th_Num^2]);
        W = zeros([Th_Num^2, 1]);
        P = zeros([1, Th_Num^2]);
    else
        W = Weight_Store{run, 1};
        P = Weight_Store{run, 2};
        Z = Weight_Store{run, 3};
    end
    % Store the start, for comparions
    Z_Init = Z;
    W_Init = W;
    P_Init = P;
    if Train
        Weight_Store_Init{run, 1} = W_Init;
        Weight_Store_Init{run, 2} = P_Init;
        Weight_Store_Init{run, 3} = Z_Init;
    end

    % Choose a random starting angle
    Theta(1,1) = (rand(1) - 0.5)*360;
    Theta(1,2) = mod(Theta(1,1) + 360, 360) - 180;

    % And create target trace
    Theta_D(1) = randperm(360, 1)-180;
    for t = 1:N 
        shift_rand = rand(1);
        if shift_rand < 1/2
            Theta_D(t+1) = mod(Theta_D(t) + 180 + Step_Size, 360) - 180;
        elseif shift_rand < 1
            Theta_D(t+1) = mod(Theta_D(t) + 180 - Step_Size, 360) - 180;
        else
            Theta_D(t+1) = Theta_D(t);
        end
    end

    % Setup first state distribution
    for angle = 1:Th_Num
        theta_dist = Angle_Dist(Theta(1,1), Theta_Cent(angle));
        for angle_d = 1:Th_Num
            theta_d_dist = Angle_Dist(Theta_D(1,1), Theta_Cent(angle_d));
            Input_Old((angle-1)*Th_Num + angle_d) = exp(-0.5*SI_Theta*(theta_dist^2 + theta_d_dist^2));
        end
    end

    Out_Old_P = P*Input_Old + randn(1)*sigma_scaler_P;
    Phi(1) = sign(Out_Old_P)/2 + 0.5;

    % And choose first fixed point
    for angle = 1:Th_Num
        Out_Old_Z(angle) = 1/(1+exp(-Z(angle,:)*Input_Old));
    end
    % Do tip to tail vector addition of these
    X = 0; Y = 0;
    for angle = 1:Th_Num
        X = X + Out_Old_Z(angle)*cos(pi*Theta_Cent(angle)/180);
        Y = Y + Out_Old_Z(angle)*sin(pi*Theta_Cent(angle)/180);
    end
    th(1) = 180*cart2pol(X,Y)/pi;
    Theta_G(1) = mod(th(1) + 180*Phi(1) + 180, 360) - 180;
    for angle = 1:Th_Num
        theta_dist = Angle_Dist(th(1), Theta_Cent(angle));
        Out_Old_Z(angle) = exp(-0.5*SI_Theta*theta_dist^2);
    end
    
    if Train ~= 0
        Theta_G(1) = randperm(360, 1) - 180;
    end
        
    % Setup intial means and things
    Out_Mean = Out_Old_Z;
    Inp_Mean = Input_Old;
    Phi_Mean = Phi(1);
    Mean_Counter = 1;
    V_OW = V_Old;
    Input_OW = Input_Old;
    T_Counter = 0;
    T_Mean = 0.5;

    % Now we loop through timesteps
    for t = 1:N    
        % If you have reached the halfway point swap learning
        if t == round(N/2)
            Alpha_P = 0;
            Alpha_Z = Alpha_Z0;
            Alpha_W = Alpha_W0;
            limit = 2;
            P_Half = P;
            Z_Half = Z;
            W_Half = W;
        end
        
        % Find the next state
        error = Theta_G(t) - Theta(t,Phi(t)+1);
        Torque(t) = Max_Torque*error/180;
        Omega(t+1) = Omega(t)*(1-eta) + Torque(t);
        Theta(t+1,:) = mod(Theta(t,:) + Omega(t+1) + 180, 360) - 180;

        % Find the resulting reward
        theta_dist = Angle_Dist(Theta(t+1,1), Theta_D(t+1));
        if theta_dist < 10
            Reward(t) = 1;
        end

        % Find how far away from the goal you are
        Dist = Angle_Dist(Theta(t+1,1), Theta_G(t));

        if Dist > Theta_Thresh && T_Counter < T_Thresh && Reward(t) < limit
            % If we are still too far away, just aim at the same angle, but
            % take the opportunity to update the value weights, why not?

            Theta_G(t+1) = Theta_G(t);
            Phi(t+1) = Phi(t);
            T_Counter = T_Counter + 1;

            if Learn
                % Create the state representation
                for angle = 1:Th_Num
                    theta_dist = Angle_Dist(Theta(t+1,1), Theta_Cent(angle));
                    for angle_d = 1:Th_Num
                        theta_d_dist = Angle_Dist(Theta_D(t+1), Theta_Cent(angle_d));
                        Input_NW((angle-1)*Th_Num + angle_d) = exp(-0.5*SI_Theta*(theta_dist^2 + theta_d_dist^2));
                    end
                end

                % Find the Value
                V_NW = W'*Input_NW;

                Delta_W(t) = Reward(t) + Gamma*V_NW - V_OW;
                W = W + Alpha_W*Delta_W(t)*Input_OW;

                Inp_Mean = t/(t+1)*Inp_Mean + Input_New/(t+1);
                V_OW = V_NW;
                Input_OW = Input_NW;
            end
        else
            % We have arrived at the goal, so update the weights all round and
            % choose a new goal
            % Create the state representation
            for angle = 1:Th_Num
                theta_dist = Angle_Dist(Theta(t+1,1), Theta_Cent(angle));
                for angle_d = 1:Th_Num
                    theta_d_dist = Angle_Dist(Theta_D(t+1), Theta_Cent(angle_d));
                    Input_New((angle-1)*Th_Num + angle_d) = exp(-0.5*SI_Theta*(theta_dist^2 + theta_d_dist^2));
                end
            end

            Out_New_P = P*Input_New + randn(1)*sigma_scaler_P;
            Outputs(t) = Out_New_P;
            Phi(t+1) = sign(Out_New_P)/2 + 0.5;

            if t < N/2 && Train ~= 0
                Theta_G(t+1) = randperm(360, 1) - 180;
            else
                % And the new fixed point
                for angle = 1:Th_Num
                    Out_New_Z(angle) = 1/(1+exp(-Z(angle,:)*Input_New));
                end
                % Do tip to tail vector addition of these
                X = 0; Y = 0;
                for angle = 1:Th_Num
                    X = X + Out_New_Z(angle)*cos(pi*Theta_Cent(angle)/180);
                    Y = Y + Out_New_Z(angle)*sin(pi*Theta_Cent(angle)/180);
                end
                th(t) = cart2pol(X,Y);
                th(t) = randn(1)*sigma_scaler_Z + 180*th(t)/pi;
                Theta_G(t+1) = mod(th(t) + 180*Phi(t+1) + 180, 360) - 180;
                for angle = 1:Th_Num
                    theta_dist = Angle_Dist(th(t), Theta_Cent(angle));
                    Out_New_Z(angle) = exp(-0.5*SI_Theta*theta_dist^2);
                end
            end

            % Find the Value
            V_New = W'*Input_New;

            if Learn
                % Update weights
                Delta_W(t) = Reward(t) + Gamma*V_New - V_OW;
                Delta_Z(t) = Reward(t) + Gamma*V_New - V_Old - (T_Counter/T_Thresh - T_Mean);
                Delta_P(t) = Reward(t) + Gamma*V_New - V_Old - 100*(T_Counter/T_Thresh - T_Mean);
                P = P + Alpha_P*Delta_P(t)*(Phi(t) - Phi_Mean)*(Input_Old - Inp_Mean)';
                W = W + Alpha_W*Delta_W(t)*Input_OW;
                Z = Z + Alpha_Z*Delta_Z(t)*(Out_Old_Z - Out_Mean)'*(Input_Old - Inp_Mean)';

                % Calculate all the running means
                Inp_Mean = t/(t+1)*Inp_Mean + Input_New/(t+1);
                Out_Mean = Mean_Counter/(Mean_Counter+1)*Out_Mean + Out_Old_Z/(Mean_Counter+1);
                T_Mean = (Mean_Counter-1)/Mean_Counter*T_Mean + T_Counter/(T_Thresh*Mean_Counter);
                Phi_Mean = Mean_Counter/(Mean_Counter+1)*Phi_Mean + Phi(t)/(Mean_Counter+1);
                Mean_Counter = Mean_Counter + 1;
            end

            % Finally relabel the old with the new!
            V_Old = V_New;
            Input_Old = Input_New;
            Out_Old_Z = Out_New_Z;
            V_OW = V_New;
            Input_OW = Input_New;
            Out_Old_P = Out_New_P;
            T_Counter = 0;
        end
    end
    
    % Now that we are done, store the weights
    Weight_Store{run, 1} = W;
    Weight_Store{run, 2} = P;
    Weight_Store{run, 3} = Z;
    
    % Now plot, first the last 50000 steps of tracking    
    subplot(Rows,Columns,(run-1)*Columns + 1)
    hold on
    xlabel('Step')
    ylabel('\Theta')
    if run == 1
        title('Angle Tracking');
    end
    xlim([N-50000,N])
    ylim([-180, 180])
    set(gca,'YTick',Theta_Tick_Labels);
    set(gca,'YTickLabel',Theta_Tick_Labels);
    Th_Place = N - 50000;
    Thd_Place = N - 50000;
    C = linspecer(2); % pretty colours
    for t = 1:N
        if abs(Theta(t+1) - Theta(t)) > 180
            plot(Th_Place:t, Theta(Th_Place:t,1), 'Linewidth', 2, 'Color', C(2,:))
            Th_Place = t+1;
        end
    end
    plot(Th_Place:t, Theta(Th_Place:t,1), 'Linewidth', 2, 'Color', C(2,:))
    for t = 1:N
        if abs(Theta_D(t+1) - Theta_D(t)) > 180
            plot(Thd_Place:t,Theta_D(Thd_Place:t), 'Linewidth', 2, 'Color', C(1,:))
            Thd_Place = t+1;
        end
    end
    plot(Thd_Place:t, Theta_D(Thd_Place:t), 'Linewidth', 2, 'Color', C(1,:))
    
    % Now the reward
    % First we are going to bin it
    subplot(Rows,Columns,(run-1)*Columns + 2)
    Reward_Binned = zeros([1,Binning]);
    for bin = 1:Binning
        Reward_Binned(bin) = mean(Reward(Bins(bin):Bins(bin+1)));
    end
    plot(Bin_Centres, Reward_Binned)
    xlabel('Step')
    ylabel('Reward')
    title('Binned Reward')
    ylim([-0.05, 1.05])
    xlim([0, N+1])

    % And then lets have a look at the final Value function etc.
    V = zeros([Th_Eval_Num,Th_Eval_Num]);
    Input = zeros([Th_Num^2, 1]);
    Phi_M = zeros([Th_Eval_Num,Th_Eval_Num]);
    Output = zeros([Th_Num, 1]);
    Goal_shift = zeros([Th_Eval_Num, Th_Eval_Num]);
    Acts = zeros([Th_Eval_Num,Th_Eval_Num]);

    for ang_inp = 1:Th_Eval_Num
        for ang_d_inp = 1:Th_Eval_Num
            for angle = 1:Th_Num
                theta_dist = Angle_Dist(Th_Grid(1,ang_inp), Theta_Cent(angle));
                for angle_d = 1:Th_Num
                    theta_d_dist = Angle_Dist(Th_Grid(1,ang_d_inp), Theta_Cent(angle_d));
                    Input((angle-1)*Th_Num + angle_d) = exp(-0.5*SI_Theta*(theta_dist^2 + theta_d_dist^2));
               end
            end
            Out = P*Input;
            Phi_M(ang_inp, ang_d_inp) = sign(Out)/2 + 0.5;
            if Phi_M(ang_inp, ang_d_inp) == 0.5
                Phi_M(ang_inp, ang_d_inp) = 0;
            end

            % Find value functions at these evaluation points
            V(ang_inp, ang_d_inp) = W'*Input;

            % Now find chosen angle for each of these points
            for angle = 1:Th_Num
                Output(angle) = 1/(1+exp(-Z(angle, :)*Input));
            end

            % Do tip to tail vector addition of these
            X = 0; Y = 0;
            for angle = 1:Th_Num
                X = X + Output(angle)*cos(pi*Theta_Cent(angle)/180);
                Y = Y + Output(angle)*sin(pi*Theta_Cent(angle)/180);
            end
            Th = 180*cart2pol(X,Y)/pi;
            Goal_shift(ang_inp, ang_d_inp) = Th;
            error = mod(Th + 180*Phi_M(ang_inp, ang_d_inp) + 180, 360) - 180 - Th_Grid(1+Phi_M(ang_inp, ang_d_inp),ang_inp);
            Acts(ang_inp, ang_d_inp) = Max_Torque*error/180;
        end
    end
    
    subplot(Rows,Columns,(run-1)*Columns + 3)
    current = subplot(Rows,Columns,(run-1)*Columns + 3);
    hold on
    imagesc(V)
    colorbar()
    current.Title.Visible='on';
    current.XLabel.Visible='on';
    current.YLabel.Visible='on';
    ylabel(current,'Theta');
    if run == 1
        title(current,'Value Function');
    elseif run == Runs
        xlabel(current,'Theta_D');
    end
    set(gca,'YTick',Theta_Ticks);
    set(gca,'YTickLabel',Theta_Tick_Labels);
    set(gca,'XTick',Theta_Ticks);
    set(gca,'XTickLabel',Theta_Tick_Labels);

    subplot(Rows,Columns,(run-1)*Columns + 4)
    current = subplot(Rows,Columns,(run-1)*Columns + 4);
    hold on
    imagesc(Goal_shift)
    colorbar()
    current.Title.Visible='on';
    current.XLabel.Visible='on';
    current.YLabel.Visible='on';
    ylabel(current,'Theta');
    if run == 1
        title(current,'Shifter Policy');
    elseif run == Runs
        xlabel(current,'Theta_D');
    end
    set(gca,'YTick',Theta_Ticks);
    set(gca,'YTickLabel',Theta_Tick_Labels);
    set(gca,'XTick',Theta_Ticks);
    set(gca,'XTickLabel',Theta_Tick_Labels);

    subplot(Rows,Columns,(run-1)*Columns + 5)
    current = subplot(Rows,Columns,(run-1)*Columns + 5);
    hold on
    imagesc(Acts)
    colorbar()
    current.Title.Visible='on';
    current.XLabel.Visible='on';
    current.YLabel.Visible='on';
    ylabel(current,'Theta');
    if run == 1
        title(current,'Final Torques');
    elseif run == Runs
        xlabel(current,'Theta_D');
    end
    set(gca,'YTick',Theta_Ticks);
    set(gca,'YTickLabel',Theta_Tick_Labels);
    set(gca,'XTick',Theta_Ticks);
    set(gca,'XTickLabel',Theta_Tick_Labels);
    
    subplot(Rows,Columns,(run-1)*Columns + 6)
    current = subplot(Rows,Columns,(run-1)*Columns + 6);
    hold on
    imagesc(Phi_M)
    colorbar()
    current.Title.Visible='on';
    current.XLabel.Visible='on';
    current.YLabel.Visible='on';
    ylabel(current,'Theta');
    if run == 1
        title(current,'Controller Choice');
    elseif run == Runs
        xlabel(current,'Theta_D');
    end
    set(gca,'YTick',Theta_Ticks);
    set(gca,'YTickLabel',Theta_Tick_Labels);
    set(gca,'XTick',Theta_Ticks);
    set(gca,'XTickLabel',Theta_Tick_Labels);
    
    subplot(Rows,Columns,(run-1)*Columns + 7)
    Reward_Ends(run) = mean(Reward(N-min(100000,N-1):N));
    if run == 1
        title('Avg Reward in last 100,000 steps:')
    end
    txt = num2str(Reward_Ends(run));
    text(0.1,0.5,txt)
end

% Save some weight matrices for nicer plotting
save('Recent_Weights.mat', 'Weight_Store', 'Weight_Store_Init')