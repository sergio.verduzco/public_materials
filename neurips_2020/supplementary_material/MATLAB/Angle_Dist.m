%% Angle_Dist.m
%
% A function to calculate the distance between two angles.

function [theta_dist] = Angle_Dist(theta_1, theta_2, signed)
theta_dist = min([abs(theta_1 - theta_2), abs(theta_1 - theta_2 + 360), abs(theta_1 - theta_2 - 360)]);
if nargin > 2
    if abs(theta_1 - theta_2) < 180
        theta_dist = theta_dist*(-1)*sign(theta_1 - theta_2);
    else
        theta_dist = theta_dist*sign(theta_1 - theta_2);
    end
end
end