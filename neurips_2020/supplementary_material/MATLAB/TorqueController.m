%% Torque_Controller.m
%
% Here we implement a neurally plausible RL agent that learns to control a
% flat pendulum with friction.
%
% It does this using an actor-critic architecture. It learns both a value
% neuron and a policy from one state representation. The value represents
% the standard discounted future reward and is learnt through standard
% dopamine type setup. The policy is implemented through a set of action
% neurons. Each code for a different torque arrayed evenly between
% -max_torque and +max_torque. Winner takes all dynamics are assumed to set
% all but the most active neuron's activity to zero, thereby selecting that
% torque. These weights are learnt via a value function modulated hebbian
% rule. Specifically:
%
% Delta_W = Step*Presynaptic_Activity*Postsynaptic_Activity*(V_New - V_Old)
%
% Part of Veduzco-Flores et al. 2020, Neurips submission, An approach to 
% synaptic learning for autonomous feedback control.

% I run and plot multiple versions at once, Runs chooses how many
% simulations to run
Runs = 3;
Reward_End = zeros([Runs,1]); % This will store the last 100000 steps of reward for each simulation

% Setup Parameters
Reinitialise = 1; % 1 if new weights, 0 to keep weights from last run
Learn = 1; % 1 to perform synaptic learn, 0 to turn it off
N = 500001; % Number of steps to run
Alpha_W0 = 0.001; % Value Learning Rate
Alpha_Z0 = 0.006; % Policy Learning Rate
Gamma = 0.99; % Reward Discount
Th_Num = 10; % Number of neurons for each dimension of input
Act_Num = 10; % Number of discrete torque options
Max_Torque = 1; % Maximum torque, 1 means speed can change by 1 deg/timestep^2 at most
eta = 0.1; % Viscous drag, effectivley the percentage of speed removed by drag in each step
S_Theta = 60; % Width of tuning curved for input layer
Step_Size = 3; % Target angle's random walk step size

% Setup some representation/neuron things
SI_Theta = 1/S_Theta^2; 
Action_Cent = -1:2/(Act_Num - 1):1; % Torques for each output neuron
Theta_Cent = -180:360/(Th_Num):180; % Centres of RBFs
Theta_Cent = Theta_Cent(1:Th_Num);
if Reinitialise 
    Weight_Store = cell(Runs, 2); % If you reinitialise you want to empty the store to keep your new weights
end

% Set up some plotting things
Th_Eval_Num = 30;
Th_Grid = -180:360/(Th_Eval_Num-1):180;
Num_Ticks = 6;
Theta_Ticks = 0:Th_Eval_Num/(Num_Ticks-1):Th_Eval_Num;
Theta_Tick_Labels = Theta_Ticks*360/Th_Eval_Num;
Rows = Runs;
Columns = 5;
figure
% I bin the reward with these
Binning = 100;
Bins = 1:N/Binning:(N+1);
Bins = round(Bins);
Bin_Centres = Bins(1:Binning) + N/(2*Binning);

for run = 1:Runs
    % Intialise variables
    Theta = zeros([N+1, 1]);
    Theta_D = zeros([N+1, 1]);
    Omega = zeros([N+1, 1]);
    Torque = zeros([N+1, 1]);
    Delta = zeros([N+1, 1]); % TD error
    Reward = zeros([N+1, 1]);
    Input_Old = zeros([Th_Num^2, 1]);
    Input_New = zeros([Th_Num^2, 1]);
    Output_New = zeros([Act_Num, 1]);
    Alpha_W = Alpha_W0; % Learning Rate
    Alpha_Z = Alpha_Z0; % Learning Rate

    if Reinitialise
        Z = randn([Act_Num, Th_Num^2]);
        W = zeros([Th_Num^2, 1]);
    else
        W = Weight_Store{run, 1};
        Z = Weight_Store{run, 2};
    end
    Z_Init = Z;
    W_Init = W;

    % Choose a random starting angle
    Theta(1) = (rand(1)- 0.5)*360;

    % And create target trace
    Theta_D(1) = randperm(360, 1)-180;
    for t = 1:N 
        shift_rand = rand(1);
        if shift_rand < 1/2
            Theta_D(t+1) = mod(Theta_D(t) + 180 + Step_Size, 360) - 180;
        elseif shift_rand < 1
            Theta_D(t+1) = mod(Theta_D(t) + 180 - Step_Size, 360) - 180;
        else
            Theta_D(t+1) = Theta_D(t);
        end
    end
    
    % Now do the first round of representation setup and action selection
    for angle = 1:Th_Num
        theta_dist = Angle_Dist(Theta(1), Theta_Cent(angle));
        for angle_d = 1:Th_Num
            theta_d_dist = Angle_Dist(Theta_D(1), Theta_Cent(angle_d));
            Input_Old((angle-1)*Th_Num + angle_d) = exp(-0.5*SI_Theta*(theta_dist^2 + theta_d_dist^2));
        end
    end
    Output_Old = Z*Input_Old;
    V_Old = W'*Input_Old;
    [~, Action_Index] = max(Output_Old);
    Output_Old(1:Act_Num ~= Action_Index) = 0;
    Torque(1) = Action_Cent(Action_Index); % Choose torque represented by most active neuron

    % loop through timesteps
    for t = 1:N
        % Halfway through we drop the learning rate to fine tune
        if t == round(N/2)
            Alpha_Z = Alpha_Z/10;
            Alpha_W = Alpha_W/10;
        end        
        
        % Update the speed and angle
        Omega(t+1) = Omega(t)*(1-eta) + Torque(t);
        Theta(t+1) = mod(Theta(t) + Omega(t+1) + 180, 360) - 180; % Angle defined between -180 and 180

        % Find the resulting reward
        theta_dist = Angle_Dist(Theta(t+1), Theta_D(t+1));
        if theta_dist < 10
            Reward(t) = 1;
        end

        % Find representation
        for angle = 1:Th_Num
            theta_dist = Angle_Dist(Theta(t+1), Theta_Cent(angle));
            for angle_d = 1:Th_Num
                theta_d_dist = Angle_Dist(Theta_D(t+1), Theta_Cent(angle_d));
                Input_New((angle-1)*Th_Num + angle_d) = exp(-0.5*SI_Theta*(theta_dist^2 + theta_d_dist^2));
            end
        end

        % Find value
        V_New = W'*Input_New;

        % Choose action
        Output_New = Z*Input_New;
        [~, Action_Index] = max(Output_New);
        Output_New(1:Act_Num ~= Action_Index) = 0;
        Torque(t+1) = Action_Cent(Action_Index);

        if Learn
            % Now get the TD error and update  weights
            Delta(t) = Reward(t) + Gamma*V_New - V_Old;
            W = W + Alpha_W*Delta(t)*Input_Old;
            Z = Z + Alpha_Z*(V_New - V_Old)*Output_Old*Input_Old';
            
            % Keep the weights bounded
            for angle = 1:Act_Num
                Z(angle,:) = Z(angle,:)/norm(Z(angle,:));
            end
        end
        
        % Out with the old, in with the new
        Output_Old = Output_New;
        Input_Old = Input_New;
        V_Old = V_New;
    end
    
    % Store the weights
    Weight_Store{run, 1} = W;
    Weight_Store{run, 2} = Z;
    
    % Now plot, first the last 50000 steps of tracking    
    subplot(Rows,Columns,(run-1)*Columns + 1)
    hold on
    xlabel('Step')
    ylabel('\Theta')
    if run == 1
        title('Angle Tracking');
    end
    xlim([N-50000,N])
    ylim([-180, 180])
    set(gca,'YTick',Theta_Tick_Labels);
    set(gca,'YTickLabel',Theta_Tick_Labels);
    Th_Place = N - 50000;
    Thd_Place = N - 50000;
    C = linspecer(2); % pretty colours
    for t = 1:N
        if abs(Theta(t+1) - Theta(t)) > 180
            plot(Th_Place:t, Theta(Th_Place:t,1), 'Linewidth', 2, 'Color', C(2,:))
            Th_Place = t+1;
        end
    end
    plot(Th_Place:t, Theta(Th_Place:t,1), 'Linewidth', 2, 'Color', C(2,:))
    for t = 1:N
        if abs(Theta_D(t+1) - Theta_D(t)) > 180
            plot(Thd_Place:t,Theta_D(Thd_Place:t), 'Linewidth', 2, 'Color', C(1,:))
            Thd_Place = t+1;
        end
    end
    plot(Thd_Place:t, Theta_D(Thd_Place:t), 'Linewidth', 2, 'Color', C(1,:))
    
    % Now plot the reward
    Reward_Binned = zeros([Binning, 1]);
    for bin = 1:Binning
        Reward_Binned(bin) = mean(Reward(Bins(bin):Bins(bin+1)));
    end
    subplot(Rows,Columns,(run-1)*Columns + 4)
    plot(Bin_Centres, Reward_Binned)
    xlabel('Step')
    ylabel('Reward')
    title('Binned Reward')
    ylim([-0.05, 1.05])
    xlim([0, N+1])
    
    % And then lets have a look at the final Value function and policy
    V = zeros([Th_Eval_Num,Th_Eval_Num]);
    Input = zeros([Th_Num^2, 1]);
    Acts = zeros([Th_Eval_Num,Th_Eval_Num]);
    Output = zeros([Th_Num, 1]);

    for ang_inp = 1:Th_Eval_Num
        for ang_d_inp = 1:Th_Eval_Num
            for angle = 1:Th_Num
                theta_dist = Angle_Dist(Th_Grid(1,ang_inp), Theta_Cent(angle));
                for angle_d = 1:Th_Num
                    theta_d_dist = Angle_Dist(Th_Grid(1,ang_d_inp), Theta_Cent(angle_d));
                    Input((angle-1)*Th_Num + angle_d) = exp(-0.5*SI_Theta*(theta_dist^2 + theta_d_dist^2));
               end
            end

            % Find value functions at these evaluation points
            V(ang_inp, ang_d_inp) = W'*Input;

            Output = Z*Input;
            [~, Action_Index] = max(Output);
            Acts(ang_inp, ang_d_inp) = Action_Cent(Action_Index);
        end
    end

    subplot(Rows,Columns,(run-1)*Columns + 2)
    current = subplot(Rows,Columns,(run-1)*Columns + 2);
    hold on
    imagesc(V)
    colorbar()
    current.Title.Visible='on';
    current.XLabel.Visible='on';
    current.YLabel.Visible='on';
    ylabel(current,'Theta');
    if run == 1
        title(current,'Value Function');
    elseif run == Runs
        xlabel(current,'Theta_D');
    end
    set(gca,'YTick',Theta_Ticks);
    set(gca,'YTickLabel',Theta_Tick_Labels);
    set(gca,'XTick',Theta_Ticks);
    set(gca,'XTickLabel',Theta_Tick_Labels);

    subplot(Rows,Columns,(run-1)*Columns + 3)
    current = subplot(Rows,Columns,(run-1)*Columns + 3);
    hold on
    imagesc(Acts)
    colorbar()
    caxis([-1,1])
    current.Title.Visible='on';
    current.XLabel.Visible='on';
    current.YLabel.Visible='on';
    ylabel(current,'Theta');
    if run == 1
        title(current,'Final Policy');
    elseif run == Runs
        xlabel(current,'Theta_D');
    end
    set(gca,'YTick',Theta_Ticks);
    set(gca,'YTickLabel',Theta_Tick_Labels);
    set(gca,'XTick',Theta_Ticks);
    set(gca,'XTickLabel',Theta_Tick_Labels);
    
    
    subplot(Rows,Columns,(run-1)*Columns + 5)
    Reward_End(run) = mean(Reward(N-100000:N));
    if run == 1
        title('Avg Reward in last 100,000 steps:')
    end
    txt = num2str(Reward_End(run));
    text(0.1,0.5,txt)
end