%% NicerPlotter.m
%
% This program plots some nicer comparison type figures between initial and
% trained weights.
%
% In order to use it we save Weight_Store and Weight_Store_Init in a .mat 
% file and load it in the line below

load Recent_Weights.mat
run = 1; % Choose which of the runs from the loaded datasets to use

% This then uses those weights to find implicit value function, policy etc.
% as well as having a look at its performance and plots it all.
%
% Part of Veduzco-Flores et al. 2020, Neurips submission, An approach to 
% synaptic learning for autonomous feedback control

% Set some parameters up, some must be consistent with the way data was
% generated.
N = 20000; % number of steps for tracking plot
Th_Num = 10; % Number of state neurons per angle
Max_Torque = 1; % Maximum torque, 1 means speed can change by 1 deg/timestep^2 at most
eta = 0.1; % Viscous drag
S_Theta = 60;
Theta_Thresh = 5; % Close enough to the target.
T_Thresh = 30; % Max number of timesteps before rechoosing angle target
Step_Size = 2; % Target angle's random walk step size

% Choose representation characteristics
SI_Theta = 1/S_Theta^2;
Theta_Cent = -180:360/(Th_Num):180;
Theta_Cent = Theta_Cent(1:Th_Num);

% Set up some plotting things
Th_Eval_Num = 40;
Th_Grid = -180:360/(Th_Eval_Num-1):180;
Th_Grid = [Th_Grid; mod(Th_Grid + 360, 360) - 180];
Num_Ticks = 5;
Theta_Ticks = 0:Th_Eval_Num/(Num_Ticks-1):Th_Eval_Num;
Theta_Tick_Labels = Theta_Ticks*360/Th_Eval_Num - 180;
Theta_Ticks(1) = 1;
figure
Rows = 4;
Columns = 2;

% Setup target, same for both intial and final weights to illustrate diff
Theta_D = zeros([N, 1]);
Theta_D(1) = randperm(360, 1)-180;
for t = 1:N 
    shift_rand = rand(1);
    if shift_rand < 1/2
        Theta_D(t+1) = mod(Theta_D(t) + 180 + Step_Size, 360) - 180;
    elseif shift_rand < 1
        Theta_D(t+1) = mod(Theta_D(t) + 180 - Step_Size, 360) - 180;
    else
        Theta_D(t+1) = Theta_D(t);
    end
end

% Run through first with initial weights, then learnt weights
for wrap = 1:2
    % Unpack weights
    if wrap == 1
        W = Weight_Store_Init{run, 1};
        P = Weight_Store_Init{run, 2};
        Z = Weight_Store_Init{run, 3};
    else
        W = Weight_Store{run, 1};
        P = Weight_Store{run, 2};
        Z = Weight_Store{run, 3};
    end

    % Initialise variables
    Theta = zeros([N+1, 2]);
    Omega = 0;
    Input = zeros([Th_Num^2, 1]);
    Output_Z = zeros([Th_Num, 1]);
    T_Counter = 0;
    
    % Choose a random starting angle
    Theta(1,1) = (rand(1) - 0.5)*360;
    Theta(1,2) = mod(Theta(1,1) + 360, 360) - 180;

    % Setup first state distribution
    for angle = 1:Th_Num
        theta_dist = Angle_Dist(Theta(1,1), Theta_Cent(angle));
        for angle_d = 1:Th_Num
            theta_d_dist = Angle_Dist(Theta_D(1,1), Theta_Cent(angle_d));
            Input((angle-1)*Th_Num + angle_d) = exp(-0.5*SI_Theta*(theta_dist^2 + theta_d_dist^2));
        end
    end

    % Choose first fixed point and controller
    Output_P = P*Input;
    Phi = sign(Output_P)/2 + 0.5;
    if Phi == 0.5
        Phi = 0;
    end

    for angle = 1:Th_Num
        Output_Z(angle) = 1/(1+exp(-Z(angle,:)*Input));
    end
    % Do tip to tail vector addition of these
    X = 0; Y = 0;
    for angle = 1:Th_Num
        X = X + Output_Z(angle)*cos(pi*Theta_Cent(angle)/180);
        Y = Y + Output_Z(angle)*sin(pi*Theta_Cent(angle)/180);
    end
    th = 180*cart2pol(X,Y)/pi;
    Theta_G = mod(th + 180*Phi + 180, 360) - 180;
    
    % Run through timesteps, generate tracking
    for t = 1:N        
        % Find the next state
        error = Theta_G - Theta(t,Phi+1);
        Torque = Max_Torque*error/180;
        Omega = Omega*(1-eta) + Torque;
        Theta(t+1,:) = mod(Theta(t,:) + Omega + 180, 360) - 180;
        
        % Find how far away from the goal you are
        Dist = Angle_Dist(Theta(t+1,1), Theta_G);
        
        if Dist > Theta_Thresh && T_Counter < T_Thresh
            % If we are still too far away, just aim at the same angle
            T_Counter = T_Counter + 1;
        else
            % Setup state distribution
            for angle = 1:Th_Num
                theta_dist = Angle_Dist(Theta(t+1,1), Theta_Cent(angle));
                for angle_d = 1:Th_Num
                    theta_d_dist = Angle_Dist(Theta_D(t+1), Theta_Cent(angle_d));
                    Input((angle-1)*Th_Num + angle_d) = exp(-0.5*SI_Theta*(theta_dist^2 + theta_d_dist^2));
                end
            end

            % Choose fixed point and controller
            Output_P = P*Input;
            Phi = sign(Output_P)/2 + 0.5;
            if Phi == 0.5
                Phi = 0;
            end

            for angle = 1:Th_Num
                Output_Z(angle) = 1/(1+exp(-Z(angle,:)*Input));
            end
            % Do tip to tail vector addition of these
            X = 0; Y = 0;
            for angle = 1:Th_Num
                X = X + Output_Z(angle)*cos(pi*Theta_Cent(angle)/180);
                Y = Y + Output_Z(angle)*sin(pi*Theta_Cent(angle)/180);
            end
            th = 180*cart2pol(X,Y)/pi;
            Theta_G = mod(th + 180*Phi + 180, 360) - 180;
            
            T_Counter = 0;
        end
    end
    
    colours = fake_parula();
    
    % Now start the plotting.
    % First the tracking
    subplot(Rows,Columns,wrap)
    hold on
    xlabel('Step')
    ylabel('\Theta')
    if wrap == 1
        title('Initial Angle Tracking');
    else
        title('Final Angle Tracking');
    end
    xlim([1,N])
    ylim([-180.001, 180])
    set(gca,'YTick',Theta_Tick_Labels);
    set(gca,'YTickLabel',Theta_Tick_Labels);
    Th_Place = 1;
    alpha = 1;
    Thd_Place = 1;
    C = linspecer(2);
    for t = 1:N
        if abs(Theta(t+1) - Theta(t)) > 180
            plot(Th_Place:t, Theta(Th_Place:t,1), 'Linewidth', 2, 'Color', [C(2,:),alpha])
            Th_Place = t+1;
        end
    end
    plot(Th_Place:t, Theta(Th_Place:t,1), 'Linewidth', 2, 'Color', [C(2,:),alpha])
    for t = 1:N
        if abs(Theta_D(t+1) - Theta_D(t)) > 180
            plot(Thd_Place:t,Theta_D(Thd_Place:t), 'Linewidth', 2, 'Color', [C(1,:),alpha])
            Thd_Place = t+1;
        end
    end
    plot(Thd_Place:t, Theta_D(Thd_Place:t), 'Linewidth', 2, 'Color', [C(1,:),alpha])

    % Then lets evaluate the network outputs at a set of grid inputs
        % And then lets have a look at the final Value function
    V = zeros([Th_Eval_Num,Th_Eval_Num]);
    Phi_M = zeros([Th_Eval_Num,Th_Eval_Num]);
    Goal_shift = zeros([Th_Eval_Num, Th_Eval_Num]);
    for ang_inp = 1:Th_Eval_Num
        for ang_d_inp = 1:Th_Eval_Num
            for angle = 1:Th_Num
                theta_dist = Angle_Dist(Th_Grid(1,ang_inp), Theta_Cent(angle));
                for angle_d = 1:Th_Num
                    theta_d_dist = Angle_Dist(Th_Grid(1,ang_d_inp), Theta_Cent(angle_d));
                    Input((angle-1)*Th_Num + angle_d) = exp(-0.5*SI_Theta*(theta_dist^2 + theta_d_dist^2));
               end
            end
            Output_P = P*Input;
            Phi_M(ang_inp, ang_d_inp) = sign(Output_P)/2 + 0.5;
            if Phi_M(ang_inp, ang_d_inp) == 0.5
                Phi_M(ang_inp, ang_d_inp) = 0;
            end

            % Find value functions at these evaluation points
            V(ang_inp, ang_d_inp) = W'*Input;

            % Now find chosen angle for each of these points
            for angle = 1:Th_Num
                Output_Z(angle) = 1/(1+exp(-Z(angle, :)*Input));
            end

            % Do tip to tail vector addition of these
            X = 0; Y = 0;
            for angle = 1:Th_Num
                X = X + Output_Z(angle)*cos(pi*Theta_Cent(angle)/180);
                Y = Y + Output_Z(angle)*sin(pi*Theta_Cent(angle)/180);
            end
            Goal_shift(ang_inp, ang_d_inp) = 180*cart2pol(X,Y)/pi;
        end
    end
    
    colormap(colours)
    
    subplot(Rows,Columns, Columns + wrap)
    current = subplot(Rows,Columns, Columns + wrap);
    hold on
    V = [V, ones(Th_Eval_Num, 1)];
    imagesc(V)
    xlim([1 Th_Eval_Num])
    ylim([1 Th_Eval_Num])
    current.Title.Visible='on';
    current.XLabel.Visible='on';
    current.YLabel.Visible='on';
    ylabel(current,'\Theta');
    xlabel(current,'\Theta_D');
    if wrap == 1
        title(current,'Initial Value Function');
    else
        title(current,'Final Value Function');
    end
    set(gca,'YTick',Theta_Ticks);
    set(gca,'YTickLabel',Theta_Tick_Labels);
    set(gca,'XTick',Theta_Ticks);
    set(gca,'XTickLabel',Theta_Tick_Labels);

    subplot(Rows,Columns, 3*Columns + wrap)
    current = subplot(Rows,Columns, 3*Columns + wrap);
    hold on
    imagesc(Goal_shift)
    xlim([1 Th_Eval_Num])
    ylim([1 Th_Eval_Num])
    colorbar()
    caxis([-180 180])
    current.Title.Visible='on';
    current.XLabel.Visible='on';
    current.YLabel.Visible='on';
    ylabel(current,'\Theta');
    xlabel(current,'\Theta_D');
    if wrap == 1
        title(current,'Initial Policy');
    else
        title(current, 'Final Policy');
    end
    set(gca,'YTick',Theta_Ticks);
    set(gca,'YTickLabel',Theta_Tick_Labels);
    set(gca,'XTick',Theta_Ticks);
    set(gca,'XTickLabel',Theta_Tick_Labels);

    subplot(Rows,Columns, 2*Columns + wrap)
    current = subplot(Rows,Columns, 2*Columns + wrap);
    hold on
    Phi_M = [Phi_M, ones(Th_Eval_Num, 1)];
    imagesc(Phi_M)
    xlim([1 Th_Eval_Num])
    ylim([1 Th_Eval_Num])
    current.Title.Visible='on';
    current.XLabel.Visible='on';
    current.YLabel.Visible='on';
    ylabel(current,'\Theta');
    xlabel(current,'\Theta_D');
    if wrap == 1
        title(current,'Initial Controller Choice');
    else
        title(current,'Final Controller Choice');
    end
    set(gca,'YTick',Theta_Ticks);
    set(gca,'YTickLabel',Theta_Tick_Labels);
    set(gca,'XTick',Theta_Ticks);
    set(gca,'XTickLabel',Theta_Tick_Labels);
end